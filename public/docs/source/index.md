---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.

<!-- END_INFO -->

#general


<!-- START_deb135bdda41f00636bdccbb1a5cbef6 -->
## Display the specified menu.

> Example request:

```bash
curl -X GET -G "http://localhost/api/menus/1" 
```

```javascript
const url = new URL("http://localhost/api/menus/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET api/menus/{menu}`


<!-- END_deb135bdda41f00636bdccbb1a5cbef6 -->

<!-- START_a626b3918c152d6b703de061fb8d61c0 -->
## Update the specified menu.

> Example request:

```bash
curl -X PUT "http://localhost/api/menus/1" 
```

```javascript
const url = new URL("http://localhost/api/menus/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/menus/{menu}`


<!-- END_a626b3918c152d6b703de061fb8d61c0 -->

<!-- START_9b9235706a133e7d084714a773ea7141 -->
## Update the specified menu.

> Example request:

```bash
curl -X PATCH "http://localhost/api/menus/1" 
```

```javascript
const url = new URL("http://localhost/api/menus/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PATCH",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PATCH api/menus/{menu}`


<!-- END_9b9235706a133e7d084714a773ea7141 -->

<!-- START_1358b72d79a29722b951d26536170549 -->
## Remove the specified menu.

> Example request:

```bash
curl -X DELETE "http://localhost/api/menus/1" 
```

```javascript
const url = new URL("http://localhost/api/menus/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/menus/{menu}`


<!-- END_1358b72d79a29722b951d26536170549 -->

<!-- START_3fffbc72811d324ba99bf7df01335e46 -->
## Store a newly created menu item.

> Example request:

```bash
curl -X POST "http://localhost/api/menus/1/items" 
```

```javascript
const url = new URL("http://localhost/api/menus/1/items");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/menus/{menu}/items`


<!-- END_3fffbc72811d324ba99bf7df01335e46 -->

<!-- START_79b2781e1d4a01b15a0b1b2df70e2a2a -->
## Display the specified menu items.

> Example request:

```bash
curl -X GET -G "http://localhost/api/menus/1/items" 
```

```javascript
const url = new URL("http://localhost/api/menus/1/items");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "field": "value",
        "children": [
            {
                "field": "value",
                "children": [
                    {
                        "field": "value",
                        "children": []
                    }
                ]
            },
            {
                "field": "value",
                "children": [
                    {
                        "field": "value",
                        "children": []
                    }
                ]
            }
        ]
    },
    {
        "field": "value",
        "children": []
    }
]
```

### HTTP Request
`GET api/menus/{menu}/items`


<!-- END_79b2781e1d4a01b15a0b1b2df70e2a2a -->

<!-- START_ae52c95a3cfe4a47332ce422418e48bc -->
## Remove the specified menu items.

> Example request:

```bash
curl -X DELETE "http://localhost/api/menus/1/items" 
```

```javascript
const url = new URL("http://localhost/api/menus/1/items");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/menus/{menu}/items`


<!-- END_ae52c95a3cfe4a47332ce422418e48bc -->

<!-- START_77194c2fe76abb0acee278518f0b0523 -->
## Display the specified layer of the menu.

> Example request:

```bash
curl -X GET -G "http://localhost/api/menus/1/layers/1" 
```

```javascript
const url = new URL("http://localhost/api/menus/1/layers/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "field": "value"
    },
    {
        "field": "value"
    }
]
```

### HTTP Request
`GET api/menus/{menu}/layers/{layer}`


<!-- END_77194c2fe76abb0acee278518f0b0523 -->

<!-- START_1d6aef0998095e44b206fb114b1d8db5 -->
## Remove the specified layer from the menu.

> Example request:

```bash
curl -X DELETE "http://localhost/api/menus/1/layers/1" 
```

```javascript
const url = new URL("http://localhost/api/menus/1/layers/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/menus/{menu}/layers/{layer}`


<!-- END_1d6aef0998095e44b206fb114b1d8db5 -->

<!-- START_1c8949c8901809506b3727d4cfd2f2b8 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET -G "http://localhost/api/menus/1/depth" 
```

```javascript
const url = new URL("http://localhost/api/menus/1/depth");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    5
]
```

### HTTP Request
`GET api/menus/{menu}/depth`


<!-- END_1c8949c8901809506b3727d4cfd2f2b8 -->

<!-- START_1f8988f8b514fb2127ba9ed8e2499f98 -->
## Display the specified item.

> Example request:

```bash
curl -X GET -G "http://localhost/api/items/1" 
```

```javascript
const url = new URL("http://localhost/api/items/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "field": "value"
}
```

### HTTP Request
`GET api/items/{item}`


<!-- END_1f8988f8b514fb2127ba9ed8e2499f98 -->

<!-- START_e34601ed139d88ac1613ec4df2056baa -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://localhost/api/items/1" 
```

```javascript
const url = new URL("http://localhost/api/items/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/items/{item}`


<!-- END_e34601ed139d88ac1613ec4df2056baa -->

<!-- START_becb1e8163f1c0de49447ba305a53a2a -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PATCH "http://localhost/api/items/1" 
```

```javascript
const url = new URL("http://localhost/api/items/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PATCH",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PATCH api/items/{item}`


<!-- END_becb1e8163f1c0de49447ba305a53a2a -->

<!-- START_4ba7e871e55098b0081507ac0b4e478b -->
## Remove the specified item.

> Example request:

```bash
curl -X DELETE "http://localhost/api/items/1" 
```

```javascript
const url = new URL("http://localhost/api/items/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/items/{item}`


<!-- END_4ba7e871e55098b0081507ac0b4e478b -->

<!-- START_384c4f191b7d941c199c42923c6a696f -->
## Store a newly created item children.

> Example request:

```bash
curl -X POST "http://localhost/api/items/1/children" 
```

```javascript
const url = new URL("http://localhost/api/items/1/children");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/items/{item}/children`


<!-- END_384c4f191b7d941c199c42923c6a696f -->

<!-- START_43ea5cc8219e95cb06c11cb05c054670 -->
## Display the specified item.

> Example request:

```bash
curl -X GET -G "http://localhost/api/items/1/children" 
```

```javascript
const url = new URL("http://localhost/api/items/1/children");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "field": "value",
        "children": [
            {
                "field": "value",
                "children": [
                    {
                        "field": "value",
                        "children": []
                    }
                ]
            },
            {
                "field": "value",
                "children": [
                    {
                        "field": "value",
                        "children": []
                    }
                ]
            }
        ]
    }
]
```

### HTTP Request
`GET api/items/{item}/children`


<!-- END_43ea5cc8219e95cb06c11cb05c054670 -->

<!-- START_342ecc992e0de222d433997a4e17725f -->
## Remove the specified item children.

> Example request:

```bash
curl -X DELETE "http://localhost/api/items/1/children" 
```

```javascript
const url = new URL("http://localhost/api/items/1/children");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/items/{item}/children`


<!-- END_342ecc992e0de222d433997a4e17725f -->


