<?php
/**
 * Php version 7.2
 *
 * ItemController File Doc Comment
 *
 * @category ItemController
 * @package  TaskMenu
 * @author   B J Fichardt <byronfich@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https: //github.com/Cobiro/task-menu
 */

namespace App\Http\Controllers;

use App\Http\Resources\ItemResource;
use App\Item;
use Illuminate\Http\Request;

/**
 * Php version 7.2
 *
 * ItemController Class Doc Comment
 *
 * @category ItemController
 * @package  TaskMenu
 * @author   B J Fichardt <byronfich@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https: //github.com/Cobiro/task-menu
 */

class ItemController extends Controller
{
    /**
     * Store a newly created item.
     *
     * @param \Illuminate\Http\Request $request request
     *
     * @return App\Http\Resources\ItemResource
     */
    public function store(Request $request)
    {
        $item = Item::create(
            [
                'field' => $request->field,
                'menu_id' => 0,
            ]
        );

        return new ItemResource($item);
    }

    /**
     * Display the specified item.
     *
     * @param int $id id of resource
     *
     * @return App\Http\Resources\ItemResource
     */
    public function show($id)
    {
        $item = Item::findOrFail($id);
        return new ItemResource($item);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request request
     * @param int                      $id      id
     *
     * @return App\Http\Resources\ItemResource
     */
    public function update(Request $request, $id)
    {
        $item = Item::findOrFail($id);
        $item->field = $request->field;
        $item->save();
        return new ItemResource($item);

    }

    /**
     * Remove the specified item.
     *
     * @param int $id id of resource
     *
     * @return void
     */
    public function destroy($id)
    {
        $item = Item::findOrFail($id);
        $item->delete();
    }
}
