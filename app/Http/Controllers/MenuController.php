<?php
/**
 * Php version 7.2
 *
 * MenuController File Doc Comment
 *
 * @category MenuController
 * @package  TaskMenu
 * @author   B J Fichardt <byronfich@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https: //github.com/Cobiro/task-menu
 */

namespace App\Http\Controllers;

use App\Http\Resources\menuSingleResource;
use App\Menu;
use Illuminate\Http\Request;

/**
 * Php version 7.2
 *
 * MenuController File Doc Comment
 *
 * @category MenuController
 * @package  TaskMenu
 * @author   B J Fichardt <byronfich@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https: //github.com/Cobiro/task-menu
 */
class MenuController extends Controller
{
    /**
     * Store a newly created menu.
     *
     * @param \Illuminate\Http\Request $request request
     *
     * @return App\Http\Resources\menuSingleResource
     */
    public function store(Request $request)
    {
        $menu = Menu::create(
            [
                'field' => $request->field,
                'max_depth' => $request->max_depth,
                'max_children' => $request->max_children,
            ]
        );

        return new menuSingleResource($menu);
    }

    /**
     * Display the specified menu.
     *
     * @param int $id id of resource
     *
     * @return App\Http\Resources\menuSingleResource
     */
    public function show($id)
    {
        $menu = Menu::findOrFail($id);
        return new menuSingleResource($id);
    }

    /**
     * Update the specified menu.
     *
     * @param \Illuminate\Http\Request $request request
     * @param int                      $id      id
     *
     * @return App\Http\Resources\menuSingleResource
     */
    public function update(Request $request, $id)
    {
        $menu = Menu::findOrFail($id);

        $menu->field = $request->field;
        $menu->max_depth = $request->max_depth;
        $menu->max_children = $request->max_children;
        $menu->save();
        return new menuSingleResource($menu);

    }

    /**
     * Remove the specified menu.
     *
     * @param int $id id of resource
     *
     * @return void
     */
    public function destroy($id)
    {
        $menu = Menu::findOrFail($id);

        $menu->delete();
    }
}
