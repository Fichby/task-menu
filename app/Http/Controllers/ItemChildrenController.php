<?php

/**
 * Php version 7.2
 *
 * ItemChildrenController File Doc Comment
 *
 * @category ItemChildrenController
 * @package  TaskMenu
 * @author   B J Fichardt <byronfich@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https: //github.com/Cobiro/task-menu
 */

namespace App\Http\Controllers;

use App\Http\Resources\MenuItemResource;
use App\Item;
use Illuminate\Http\Request;

/**
 * ItemChildrenController Class Doc Comment
 *
 * @category ItemChildrenController
 * @package  TaskMenu
 * @author   B J Fichardt <byronfich@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https: //github.com/Cobiro/task-menu
 */

class ItemChildrenController extends Controller
{

    /**
     * Store a newly created item children.
     *
     * @param \Illuminate\Http\Request $request request
     * @param int                      $id      id of the resource
     *
     * @return App\Http\Resources\MenuItemResource
     */
    public function store(Request $request, $id)
    {
        foreach ($request->all() as $key => $children) {
            $item = Item::create(
                [
                    'field' => $children['field'],
                    'parent_id' => $id,
                    'layer' => 0,
                    'menu_id' => 0,
                ]
            );
            if (isset($children['children']) && count($children['children']) > 0) {

                $this->createchild($children, $item->id);
            }

        }
        $items = Item::where('id', $id)->get();
        return MenuItemResource::collection($items);

    }

    /**
     * Helper function to Store a newly created item children.
     *
     * @param mixed $children children of item
     * @param int   $item     item id
     *
     * @return void
     */
    public function createchild($children, int $item)
    {

        foreach ($children['children'] as $key => $child) {

            $newitem = Item::create(
                [
                    'menu_id' => 0,
                    'field' => $child['field'],
                    'layer' => 0,
                    'parent_id' => $item,
                ]
            );

            if (isset($child['children']) && count($child['children']) > 0) {

                $this->createchild($child, $newitem->id);

            }
        }

    }

    /**
     * Display the specified item.
     *
     * @param int $id id of resource
     *
     * @return App\Http\Resources\MenuItemResource
     */
    public function show($id)
    {
        $items = Item::where('id', $id)->get();
        return MenuItemResource::collection($items);

    }

    /**
     * Remove the specified item children.
     *
     * @param int $id id of resource
     *
     * @return void
     */
    public function destroy($id)
    {
        Item::where('id', $id)->delete();

    }
}
