<?php
/**
 * Php version 7.2
 *
 * MenuDepthController File Doc Comment
 *
 * @category MenuDepthController
 * @package  TaskMenu
 * @author   B J Fichardt <byronfich@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https: //github.com/Cobiro/task-menu
 */

namespace App\Http\Controllers;

use App\Menu;

/**
 * Php version 7.2
 *
 * MenuDepthController File Doc Comment
 *
 * @category MenuDepthController
 * @package  TaskMenu
 * @author   B J Fichardt <byronfich@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https: //github.com/Cobiro/task-menu
 */
class MenuDepthController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param int $id id of resource
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Menu::findOrFail($id)->pluck('max_depth');

    }
}
