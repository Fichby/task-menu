<?php
/**
 * Php version 7.2
 *
 * MenuLayerController File Doc Comment
 *
 * @category MenuLayerController
 * @package  TaskMenu
 * @author   B J Fichardt <byronfich@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https: //github.com/Cobiro/task-menu
 */

namespace App\Http\Controllers;

use App\Http\Resources\ItemResource;
use App\Menu;

/**
 * Php version 7.2
 *
 * MenuLayerController Class Doc Comment
 *
 * @category MenuLayerController
 * @package  TaskMenu
 * @author   B J Fichardt <byronfich@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https: //github.com/Cobiro/task-menu
 */
class MenuLayerController extends Controller
{
    /**
     * Display the specified layer of the menu.
     *
     * @param int $id    id of resource
     * @param int $layer id of resource
     *
     * @return App\Http\Resources\ItemResource
     */
    public function show($id, $layer)
    {
        $menu = Menu::findOrFail($id);
        $item = $menu->items()->where('layer', $layer)->get();
        return ItemResource::collection($item);

    }

    /**
     * Remove the specified layer from the menu.
     *
     * @param int $id    id of resource
     * @param int $layer id of resource
     *
     * @return void
     */
    public function destroy($id, $layer)
    {
        $menu = Menu::findOrFail($id);
        $items = $menu->items()->get();
        $parent_id = null;
        foreach ($items as $key => $item) {
            if ($item->layer == $layer) {
                $parent_id = $item->parent_id;
            } elseif ($item->layer > $layer) {
                $item->parent_id = $parent_id;
                $item->layer--;
                $item->save();
            }
        }
        $menu->items()->where('layer', $layer)->delete();

    }
}
