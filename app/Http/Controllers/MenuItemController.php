<?php
/**
 * Php version 7.2
 *
 * MenuItemController File Doc Comment
 *
 * @category MenuItemController
 * @package  TaskMenu
 * @author   B J Fichardt <byronfich@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https: //github.com/Cobiro/task-menu
 */

namespace App\Http\Controllers;

use App\Http\Resources\MenuItemResource;
use App\Menu;
use Illuminate\Http\Request;

/**
 * Php version 7.2
 *
 * MenuItemController File Doc Comment
 *
 * @category MenuItemController
 * @package  TaskMenu
 * @author   B J Fichardt <byronfich@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https: //github.com/Cobiro/task-menu
 */
class MenuItemController extends Controller
{
    protected $menucounter;

    /**
     * Initialize menucounetr.
     */
    public function __construct()
    {
        $this->menucounter = 1;
    }

    /**
     * Store a newly created menu item.
     *
     * @param \Illuminate\Http\Request $request request
     * @param int                      $id      id of resource
     *
     * @return App\Http\Resources\MenuItemResource
     */
    public function store(Request $request, $id)
    {

        $menu = Menu::findOrFail($id);

        foreach ($request->all() as $key => $menuitem) {

            $item = $menu->items()->create(
                [
                    'menu_id' => $menu->id,
                    'field' => $menuitem['field'],
                    'layer' => $this->menucounter,
                    'parent_id' => null,
                ]
            );

            if (isset($menuitem['children']) && count($menuitem['children']) > 0) {
                $this->menucounter++;
                $this->createchild($menu, $menuitem, $item->id);
                $this->menucounter--;

            }

        }

        return MenuItemResource::collection($menu->items->where('parent_id', null));
    }

    /**
     * Helper function to Store a newly created menu item children.
     *
     * @param mixed $menu     menu object
     * @param mixed $menuitem menuitem object
     * @param int   $item     id of resource
     *
     * @return void
     */
    public function createchild($menu, $menuitem, int $item)
    {
        if ($menu->max_depth < $this->menucounter) {
            return "Max menu depth exceeded";

        }
        if (count($menuitem['children']) > $menu->max_children) {
            return "Max menu children exceeded";

        }
        foreach ($menuitem['children'] as $key => $menuchild) {

            $newitem = $menu->items()->create(
                [
                    'menu_id' => $menu->id,
                    'field' => $menuchild['field'],
                    'layer' => $this->menucounter,
                    'parent_id' => $item,
                ]
            );

            if (isset($menuchild['children']) && count($menuchild['children']) > 0) {
                $this->menucounter++;
                $this->createchild($menu, $menuchild, $newitem->id);
                $this->menucounter--;

            }
        }

    }

    /**
     * Display the specified menu items.
     *
     * @param int $id id of resource
     *
     * @return App\Http\Resources\MenuItemResource
     */
    public function show($id)
    {
        $menu = Menu::findOrFail($id);

        return MenuItemResource::collection($menu->items->where('parent_id', null));
    }

    /**
     * Remove the specified menu items.
     *
     * @param int $id id of resource
     *
     * @return void
     */
    public function destroy($id)
    {
        Menu::where('menu_id', $id)->delete();
    }
}
