<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MenuItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (!empty($this->children)) {
            return [
                'field' => $this->field,
                'children' => MenuItemResource::collection($this->children),
            ];

        } else {
            return [
                'field' => $this->field,

            ];

        }

    }
}
